﻿using Microsoft.AspNetCore.Mvc;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.EventManager.Business;

namespace TTG.Web.EventManager.Api.Controllers
{
    [Route("EventManager/[controller]")]
    [ApiController]
    public class GetController : ControllerBase
    {
        private readonly IEventManager _eventManager;
        private readonly IVenueManager _venueManager;
        private readonly IPackageManager _packageManager;

        public GetController(IEventManager eventManager, IVenueManager venueManager, IPackageManager packageManager)
        {
            _eventManager = eventManager;
            _venueManager = venueManager;
            _packageManager = packageManager;
        }

        [HttpGet]
        [Route("EventById")]
        public async Task<ActionResult<Response<List<Event>>>> EventById(int eventId)
        {
            try
            {
                var result = await _eventManager.GetEventByID(eventId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("VenueById")]
        public async Task<ActionResult<Response<List<Venue>>>> VenueById(int venueId)
        {
            try
            {
                var result = await _venueManager.GetVenueByID(venueId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("PackageById")]
        public async Task<ActionResult<Response<List<Package>>>> PackageById(int packageId)
        {
            try
            {
                var result = await _packageManager.GetPackageByID(packageId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("EventByName")]
        public async Task<ActionResult<Response<List<Event>>>> EventByName(string eventName)
        {
            try
            {
                var result = await _eventManager.GetEventByName(eventName);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("VenueByName")]
        public async Task<ActionResult<Response<List<Venue>>>> VenueByName(string venueName)
        {
            try
            {
                var result = await _venueManager.GetVenueByName(venueName);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("AllVenues")]
        public async Task<ActionResult<Response<List<Venue>>>> AllVenues()
        {
            try
            {
                var result = await _venueManager.GetAllVenues();
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("AllEvents")]
        public async Task<ActionResult<Response<List<Venue>>>> AllEvents()
        {
            try
            {
                var result = await _eventManager.GetAllEvents();
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }

        [HttpGet]
        [Route("AllPackages")]
        public async Task<ActionResult<Response<List<Venue>>>> AllPackages()
        {
            try
            {
                var result = await _packageManager.GetAllSystemPackages();
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new
                {
                    IsSuccesful = false,
                    Data = false,
                    Ex = ex
                };
                return Ok(result);
            }
        }
    }
}
