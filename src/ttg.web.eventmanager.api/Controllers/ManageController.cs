﻿using Microsoft.AspNetCore.Mvc;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.EventManager.Business;

namespace TTG.Web.EventManager.Api.Controllers
{
    [Route("EventManager/[controller]")]
    [ApiController]
    public class ManageController : ControllerBase
    {
        private readonly IEventManager _eventManager;
        private readonly IVenueManager _venueManager;
        private readonly IPackageManager _packageManager;

        public ManageController(IEventManager eventManager, IVenueManager venueManager, IPackageManager packageManager)
        {
            _eventManager = eventManager;
            _venueManager = venueManager;
            _packageManager = packageManager;
        }

        [HttpPut]
        [Route("UpdateEvent")]
        public async Task<ActionResult<Response<Event>>> UpdateEvent(Request<Event> input)
        {
            try
            {
                var result = await _eventManager.UpdateEvent(input.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }

        [HttpPut]
        [Route("UpdateVenue")]
        public async Task<ActionResult<Response<Venue>>> UpdateVenue(Request<Venue> input)
        {
            try
            {
                var result = await _venueManager.UpdateVenue(input.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }


        [HttpPut]
        [Route("UpdatePackage")]
        public async Task<ActionResult<Response<Package>>> UpdatePackage(Request<Package> package)
        {
            try
            {
                var result = await _packageManager.UpdatePackage(package.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }

        [HttpDelete]
        [Route("DeleteEvent")]
        public async Task<ActionResult<Response<Event>>> DeleteEvent(int eventId)
        {
            try
            {
                var result = await _eventManager.DeleteEvent(eventId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }

        [HttpDelete]
        [Route("DeleteVenue")]
        public async Task<ActionResult<Response<Venue>>> DeleteVenue(int venueId)
        {
            try
            {
                var result = await _venueManager.DeleteVenue(venueId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }


        [HttpDelete]
        [Route("DeletePackage")]
        public async Task<ActionResult<Response<Package>>> DeletePackage(int packageId)
        {
            try
            {
                var result = await _packageManager.DeletePackage(packageId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }
    }
}
