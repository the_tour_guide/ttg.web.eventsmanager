﻿using Microsoft.AspNetCore.Mvc;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.EventManager.Business;

namespace TTG.Web.EventManager.Api.Controllers
{
    [Route("EventManager/[controller]")]
    [ApiController]
    public class CreateController : ControllerBase
    {
        private readonly IEventManager _eventManager;
        private readonly IVenueManager _venueManager;
        private readonly IPackageManager _packageManager;

        public CreateController(IEventManager eventManager, IVenueManager venueManager, IPackageManager packageManager)
        {
            _eventManager = eventManager;
            _venueManager = venueManager;
            _packageManager = packageManager;
        }

        [HttpPost]
        [Route("CreateEvent")]
        public async Task<ActionResult<Response<Event>>> CreateEvent(Request<Event> input)
        {
            try
            {
                var result = await _eventManager.CreateEvent(input.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("CreateVenue")]
        public async Task<ActionResult<Response<Venue>>> CreateVenue(Request<Venue> input)
        {
            try
            {
                var result = await _venueManager.CreateVenue(input.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }

        [HttpPost]
        [Route("CreatePackage")]
        public async Task<ActionResult<Response<Package>>> CreatePackage(Request<Package> input)
        {
            try
            {
                var result = await _packageManager.CreatePackage(input.Data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                var result = new Response<bool>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
                return Ok(result);
            }
        }
    }
}
