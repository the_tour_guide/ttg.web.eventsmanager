﻿using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Data.Repository;
using TTG.Web.EventManager.Business;
using TTG.Web.EventManager.Repository;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<TTGDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Add DI
builder.Services.AddTransient<IEventManager, EventManager>();
builder.Services.AddTransient<IVenueManager, VenueManager>();
builder.Services.AddTransient<IPackageManager, PackageManager>();

builder.Services.AddTransient<IEventRepository, EventRepository>();
builder.Services.AddTransient<IVenueRepository, VenueRepository>();
builder.Services.AddTransient<IPackageRepository, PackageRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

