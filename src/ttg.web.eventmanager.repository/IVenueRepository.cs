﻿
using TTG.Web.Core.Common.Models;

namespace TTG.Web.EventManager.Repository
{
    public interface IVenueRepository
    {
        Task<int> CreateVenue(Venue venueDetails);
        Task<int> DeleteVenue(int venueId);
        Task<Venue> GetVenueById(int venueId);
        Task<List<Venue>> GetVenueByName(string venueName);
        Task<List<Venue>> GetAllVenues();
        Task<int> UpdateVenue(Venue venueDetails);
    }
}