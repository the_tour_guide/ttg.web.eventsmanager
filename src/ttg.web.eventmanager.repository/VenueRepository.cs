﻿using System;
using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Data.Repository;

namespace TTG.Web.EventManager.Repository
{
    public class VenueRepository : IVenueRepository
    {
        private readonly TTGDbContext _context;
        public VenueRepository(TTGDbContext context)
        {
            _context = context;
        }

        public async Task<int> CreateVenue(Venue venueDetails)
        {
            try
            {
                _context.Add(venueDetails);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return 0;
                throw new Exception("Something broke when creating a venue with Id: " + venueDetails.VenueId, ex);
            }
        }

        public async Task<Venue> GetVenueById(int venueId)
        {
            try
            {
                var table = _context.Venues;
                var _venue = await table.Where(k => k.VenueId == venueId).FirstOrDefaultAsync();
                return _venue;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting a venue with Id: " + venueId, ex);
            }
        }

        public async Task<List<Venue>> GetVenueByName(string venueName)
        {
            try
            {
                var _venue = await _context.Venues.Where(k => EF.Functions.Like(k.Name, venueName)).ToListAsync();
                return _venue;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting a venue with name like: " + venueName, ex);
            }
        }

        public async Task<List<Venue>> GetAllVenues()
        {
            try
            {
                var _venue = await _context.Venues.ToListAsync();
                return _venue;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting all venues.", ex);
            }
        }

        public async Task<int> DeleteVenue(int venueId)
        {
            try
            {
                _context.Venues.Remove(new Venue { VenueId = venueId });
                var result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting a venue with Id: " + venueId, ex);
            }
        }

        public async Task<int> UpdateVenue(Venue venueDetails)
        {
            try
            {
                _context.Venues.Update(venueDetails);
                var result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when updating an venue with Id: " + venueDetails.VenueId, ex);
            }
        }
    }
}

