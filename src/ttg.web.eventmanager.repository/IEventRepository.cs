﻿using TTG.Web.Core.Common.Models;

namespace TTG.Web.EventManager.Repository
{
    public interface IEventRepository
    {
        Task<int> CreateEvent(Event eventDetails);
        Task<Event> GetEventById(int eventId);
        Task<List<Event>> GetEventByName(string eventName);
        Task<List<Event>> GetAllEvents();
        Task<int> UpdateEvent(Event eventDetails);
        Task<int> DeleteEvent(int eventId);
    }
}