﻿using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Data.Repository;

namespace TTG.Web.EventManager.Repository
{
    public class PackageRepository : IPackageRepository
    {
        private readonly TTGDbContext _context;

        public PackageRepository(TTGDbContext context)
        {
            _context = context;
        }

        public async Task<int> CreatePackage(Package packageDetails)
        {
            try
            {
                _context.Add(packageDetails);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return 0;
                throw new Exception("Something broke when creating a package with Id: " + packageDetails.EventId, ex);
            }
        }

        public async Task<Package> GetPackageById(int packageId)
        {
            try
            {
                var _package = await _context.Packages.Where(k => k.EventId == packageId).FirstOrDefaultAsync();
                return _package;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting a package with Id: " + packageId, ex);
            }
        }

        public async Task<List<Package>> GetAllSystemPackages()
        {
            try
            {
                var _package = await _context.Packages.ToListAsync();
                return _package;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting all sys packages.", ex);
            }
        }
        
        public async Task<int> DeletePackage(int packageId)
        {
            try
            {
                _context.Packages.Remove(new Package { EventId = packageId });
                var result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting a package with Id: " + packageId, ex);
            }
        }

        public async Task<int> UpdatePackage(Package packageDetails)
        {
            try
            {
                _context.Packages.Update(packageDetails);
                var result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when updating a package with Id: " + packageDetails.EventId, ex);
            }
        }
    }
}

