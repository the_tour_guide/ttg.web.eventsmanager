﻿using TTG.Web.Core.Common.Models;

namespace TTG.Web.EventManager.Repository
{
    public interface IPackageRepository
    {
        Task<int> CreatePackage(Package packageDetails);
        Task<int> DeletePackage(int packageId);
        Task<Package> GetPackageById(int packageId);
        Task<int> UpdatePackage(Package packageDetails);
        Task<List<Package>> GetAllSystemPackages();
    }
}