﻿using Microsoft.EntityFrameworkCore;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Data.Repository;

namespace TTG.Web.EventManager.Repository
{
    public class EventRepository : IEventRepository
    {
        private readonly TTGDbContext _context;

        public EventRepository(TTGDbContext context)
        {
            _context = context;
        }

        public async Task<int> CreateEvent(Event eventDetails)
        {
            try
            {
                _context.Add(eventDetails);
                return await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return 0;
                throw new Exception("Something broke when creating an event with Id: " + eventDetails.EventId, ex);
            }
        }

        public async Task<Event> GetEventById(int eventId)
        {
            try
            {
                var _event = await _context.Events.Where(k => k.EventId == eventId).FirstOrDefaultAsync();
                return _event;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting an event with Id: " + eventId, ex);
            }
        }

        public async Task<List<Event>> GetEventByName(string eventName)
        {
            try
            {
                var _venue = await _context.Events.Where(eventItem => EF.Functions.Like(eventItem.Name, eventName)).ToListAsync();
                return _venue;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting an event with name like: " + eventName, ex);
            }
        }

        public async Task<List<Event>> GetAllEvents()
        {
            try
            {
                var _venue = await _context.Events.ToListAsync();
                return _venue;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting all events", ex);
            }
        }

        public async Task<int> DeleteEvent(int eventId)
        {
            try
            {
                _context.Events.Remove(new Event { EventId = eventId });
                var result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when getting an event with Id: " + eventId, ex);
            }
        }

        public async Task<int> UpdateEvent(Event eventDetails)
        {
            try
            {
                _context.Events.Update(eventDetails);
                var result = await _context.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Something broke when updating an event with Id: " + eventDetails.EventId, ex);
            }
        }
    }
}

