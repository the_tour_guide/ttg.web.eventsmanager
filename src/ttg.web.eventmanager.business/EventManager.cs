﻿using System;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.EventManager.Repository;

namespace TTG.Web.EventManager.Business
{
	public class EventManager : IEventManager
	{
        private readonly IEventRepository _eventRepository;

        public EventManager(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<Response> CreateEvent(Event eventDetails)
        {
            try
            {
                var result = await _eventRepository.CreateEvent(eventDetails);
                return new Response<Event>
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response<Event>> GetEventByID(int eventId)
        {
            try
            {
                var result = await _eventRepository.GetEventById(eventId);
                return new Response<Event>
                {
                    Data = result,
                    IsSuccesful = true
                };

            }
            catch (Exception ex)
            {
                return new Response<Event>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response<List<Event>>> GetEventByName(string eventName)
        {
            try
            {
                var result = await _eventRepository.GetEventByName(eventName);
                return new Response<List<Event>>
                {
                    Data = result,
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Event>>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response<List<Event>>> GetAllEvents()
        {
            try
            {
                var result = await _eventRepository.GetAllEvents();
                return new Response<List<Event>>
                {
                    Data = result,
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response<List<Event>>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        } 

        public async Task<Response> DeleteEvent(int eventId)
        {
            try
            {
                var result = await _eventRepository.DeleteEvent(eventId);
                return new Response<Event>
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> UpdateEvent(Event eventDetails)
        {
            try
            {
                var result = await _eventRepository.UpdateEvent(eventDetails);
                return new Response
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }       
    }
}


