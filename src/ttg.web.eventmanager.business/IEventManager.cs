﻿using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.EventManager.Business
{
	public interface IEventManager
	{
		Task<Response<Event>> GetEventByID(int ID);
		Task<Response<List<Event>>> GetEventByName(string eventName);
		Task<Response<List<Event>>> GetAllEvents();
		Task<Response> CreateEvent(Event eventDetails);
		Task<Response> UpdateEvent(Event eventDetails);
		Task<Response> DeleteEvent(int eventId);
	}
}

