﻿using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.EventManager.Business
{
    public interface IPackageManager
    {
        Task<Response> CreatePackage(Package packageDetails);
        Task<Response> DeletePackage(int packageId);
        Task<Response<List<Package>>> GetAllSystemPackages();
        Task<Response<Package>> GetPackageByID(int packageId);
        Task<Response> UpdatePackage(Package packageDetails);
    }
}