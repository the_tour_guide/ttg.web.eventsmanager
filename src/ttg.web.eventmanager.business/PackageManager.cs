﻿using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.EventManager.Repository;

namespace TTG.Web.EventManager.Business
{
    public class PackageManager : IPackageManager
    {
        private readonly IPackageRepository _packageRepository;

        public PackageManager(IPackageRepository packageRepository)
        {
            _packageRepository = packageRepository;
        }

        public async Task<Response> CreatePackage(Package packageDetails)
        {
            try
            {
                var result = await _packageRepository.CreatePackage(packageDetails);
                return new Response<Package>
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response<Package>> GetPackageByID(int packageId)
        {
            try
            {
                var result = await _packageRepository.GetPackageById(packageId);
                return new Response<Package>
                {
                    Data = result,
                    IsSuccesful = true
                };

            }
            catch (Exception ex)
            {
                return new Response<Package>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response<List<Package>>> GetAllSystemPackages()
        {
            try
            {
                var result = await _packageRepository.GetAllSystemPackages();
                return new Response<List<Package>>
                {
                    Data = result,
                    IsSuccesful = true
                };

            }
            catch (Exception ex)
            {
                return new Response<List<Package>>
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> DeletePackage(int packageId)
        {
            try
            {
                var result = await _packageRepository.DeletePackage(packageId);
                return new Response<Package>
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }

        public async Task<Response> UpdatePackage(Package packageDetails)
        {
            try
            {
                var result = await _packageRepository.UpdatePackage(packageDetails);
                return new Response
                {
                    IsSuccesful = true
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
        }
    }
}

