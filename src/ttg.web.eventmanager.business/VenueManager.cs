﻿using System;
using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;
using TTG.Web.EventManager.Repository;

namespace TTG.Web.EventManager.Business
{
	public class VenueManager: IVenueManager
	{
		private readonly IVenueRepository _venueRepository;

		public VenueManager(IVenueRepository venueRepository)
		{
			_venueRepository = venueRepository;
		}

		public async Task<Response> CreateVenue(Venue venueDetails)
		{
			try
			{
				var result = await _venueRepository.CreateVenue(venueDetails);
				return new Response<Venue>
				{
					IsSuccesful = true
				};
			}
			catch (Exception ex)
			{
				return new Response
				{
					IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
			}
		}

		public async Task<Response<Venue>> GetVenueByID(int venueId)
		{
            try
            {
				var result = await _venueRepository.GetVenueById(venueId);
				return new Response<Venue>
				{
					Data = result,
					IsSuccesful = true
				};

            }
            catch (Exception ex)
            {
				return new Response<Venue>
				{
					IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
            }
		}

		public async Task<Response<List<Venue>>> GetVenueByName(string venueName)
		{
            try
            {
				var result = await _venueRepository.GetVenueByName(venueName);
				return new Response<List<Venue>>
				{
					Data = result,
					IsSuccesful = true
				};
			}
            catch (Exception ex)
            {
				return new Response<List<Venue>>
				{
					IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
			}
		}

		public async Task<Response<List<Venue>>> GetAllVenues()
		{
			try
			{
				var result = await _venueRepository.GetAllVenues();
				return new Response<List<Venue>>
				{
					Data = result,
					IsSuccesful = true
				};
			}
			catch (Exception ex)
			{
				return new Response<List<Venue>>
				{
					IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
			}
		}

		public async Task<Response> UpdateVenue(Venue venueDetails)
		{
            try
            {
				var result = await _venueRepository.UpdateVenue(venueDetails);
				return new Response
				{
					IsSuccesful = true
				};
			}
            catch (Exception ex)
            {
				return new Response
				{
					IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
			}
		}

		public async Task<Response> DeleteVenue(int venueId)
		{
			try
			{
				var result = await _venueRepository.DeleteVenue(venueId);
				return new Response<Venue>
				{
					IsSuccesful = true
				};
			}
			catch (Exception ex)
			{
				return new Response
				{
					IsSuccesful = false,
                    ErrorMessage = ex.Message
                };
			}
		}
	}
}

