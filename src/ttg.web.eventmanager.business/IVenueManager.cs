﻿using TTG.Web.Core.Common.Models;
using TTG.Web.Core.Common.Models.Http;

namespace TTG.Web.EventManager.Business
{
	public interface IVenueManager
	{
		Task<Response> CreateVenue(Venue venueDetails);
		Task<Response<Venue>> GetVenueByID(int venueID);
		Task<Response<List<Venue>>> GetVenueByName(string venueName);
		Task<Response<List<Venue>>> GetAllVenues();
		Task<Response> DeleteVenue(int VenueID);
		Task<Response> UpdateVenue(Venue venueDetails);
	}
}

